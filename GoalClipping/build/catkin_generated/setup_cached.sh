#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/qbyk/GoalClipping/devel:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/qbyk/GoalClipping/devel/lib:$LD_LIBRARY_PATH"
export PKG_CONFIG_PATH="/home/qbyk/GoalClipping/devel/lib/pkgconfig:$PKG_CONFIG_PATH"
export PWD='/home/qbyk/GoalClipping/build'
export ROSLISP_PACKAGE_DIRECTORIES='/home/qbyk/GoalClipping/devel/share/common-lisp'
export ROS_PACKAGE_PATH="/home/qbyk/GoalClipping/src:$ROS_PACKAGE_PATH"