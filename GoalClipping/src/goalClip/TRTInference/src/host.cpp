
#include "../include/host.h"

Host::Host()
{
    Config config;
    config.TRTknow(orig, goal);
    config.TRTpath(first, second, video, engine, onnx);

    cv::namedWindow("test", cv::WINDOW_NORMAL);
    resizeWindow("test", 360, 340);
    createTrackbar("speed", "test", speed, 5);

    cv::namedWindow("orig", cv::WINDOW_NORMAL);
    resizeWindow("orig", 720, 480);
    createTrackbar("save", "orig", save, 2);
    num += orig.size();
    time += goal.size();
}
void Host::spin()
{
    Mainprocess();
}
void Host::Mainprocess()
{
    TRTInferV1::TRTInfer myInfer(0);
    if (!myInfer.initModule(engine, 1, 1))
    {
        nvinfer1::IHostMemory *data;
        data = myInfer.createEngine(onnx, 4, 1280, 1280);
        myInfer.saveEngineFile(data, engine);
        myInfer.initModule(engine, 1, 1);
    }
    std::vector<cv::Mat> frames;
    std::vector<cv::Mat> img;
    Mat src;
    Mat src_i;
    cv::VideoCapture capture(video);

    while (true)
    {
        frames.clear();

        capture >> src;
        frames.emplace_back(src.clone());
         img.emplace_back(src);
        std::vector<std::vector<TRTInferV1::DetectionObj>> result = myInfer.doInference(frames, 0.1, 0.45, 0.3);
        for (int i(0); i < int(frames.size()); ++i)
        {
            int flag=0;
            for (int j(0); j < int(result[i].size()); ++j)
            {
                cv::Rect r = cv::Rect(result[i][j].x1, result[i][j].y1, result[i][j].x2 - result[i][j].x1, result[i][j].y2 - result[i][j].y1);
                cv::rectangle(frames[i], r, cv::Scalar(255, 255, 255), 5);
            }
            if (way != 0)
            {
                imshow("orig", frames[i]);
                waitKey(47);
            }
            for (int k(0); k < int(result[i].size()); ++k)
            {
                int mode = getTrackbarPos("speed", "test");
                if (mode == 0)
                    return;
                if (mode == 1)
                    waitTime = 0;
                if (mode == 2)
                    waitTime = 3000;
                if (mode == 3)
                    waitTime = 2000;
                if (mode == 4)
                    waitTime = 1000;
                if (mode == 5)
                    waitTime = 50;
                if (way != 0)
                {
                    way = getTrackbarPos("save", "orig");
                }
                cv::Rect rect = cv::Rect(result[i][k].x1, result[i][k].y1, result[i][k].x2 - result[i][k].x1, result[i][k].y2 - result[i][k].y1);
                cv::rectangle(frames[i], rect, cv::Scalar(255, 0, 255), 7);
                Mat dest = src(rect);
                resize(dest, dest, Size(640, 640), 0, 0, INTER_CUBIC);
                imshow("test", dest);

                if (way != 0)
                {
                    imshow("orig", frames[i]);
                }
                else
                {
                    cv::destroyWindow("orig");
                }
                if (waitKey(waitTime) != 'd')
                {
                    string OutputPath = second + to_string(time) + ".png";
                    imwrite(OutputPath, dest);
                    cv::rectangle(frames[i], rect, cv::Scalar(255, 0, 0), 7);
                    flag=1;
                    cout << "Save->" << OutputPath << endl;
                    time++;
                }
                else
                {
                    cv::rectangle(frames[i], rect, cv::Scalar(0, 0, 255), 7);
                    cout << "Being->delate" << endl;
                }
            }
            if (way != 1&&flag==1)
            {
                string firstPath = first + to_string(num) + ".png";
                imwrite(firstPath, img[i]);
                num++;
            }
        }
    }
}
Host::~Host()
{
    cv::destroyWindow("test");
    if (way != 0)
    {
        cv::destroyWindow("orig");
    }
}