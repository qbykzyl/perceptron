#include "Inference.h"
#include <ros/ros.h>
#include "../../config/config.h"
#include <cstring>
#include <string>
using namespace std;
using namespace cv;
using namespace  TRTInferV1;
class Host
{
    int time=0;
    int num=0;

    int a=1;
    int *speed=&a;
    int waitTime;

    int b=1;
    int *save=&b;
    int way=1;
    vector<string> orig;
    vector<string> goal;
    string first;
    string second;
    string video;
    string engine;
    string onnx;

public:
    Host();
    ~Host();
    void spin();
    void Mainprocess();

};