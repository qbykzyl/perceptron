#include "include/host.h"
using namespace std;
int main(int argc, char **argv)
{
    ros::init(argc, argv, "GoClp");
    Host host;
    host.spin();
    return 0;
}