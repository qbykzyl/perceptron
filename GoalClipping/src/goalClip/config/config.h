#include <cstring>
#include <string>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <fstream>
#include <sstream>
#include <vector>
using namespace std;
using namespace cv;
class Config
{
    // TRT相关
    std::string TRTVideoPath = "/home/qbyk/B.mp4";                    // 推理视频地址
    std::string videoPath = "/home/qbyk/GoalClipping/orig/"; // 视频图片保存路径
    std::string Opath = videoPath + "*png";                  // 用于读取目标保存文件里有多少图片
    std::string Engine = "/home/qbyk/infer_ws/src/infer/src/TRTInference/sample/engines/model_trt_c.engine";
    std::string Onnx = "/home/ninefish/nine-fish/TRTInferenceForYolo/sample/build/best.onnx";
    // labelGo相关
    std::string InputPath = "/home/qbyk/data/*.png"; // 用labelGo标记好的文件
    std::string Inputtxts = "/home/qbyk/data/*.txt"; // 用labelGo标记好的TXT文件地址
    // 目标相关
    std::string path = "/home/qbyk/GoalClipping/goal/"; // 剪切后目标存储地址
    std::string writePath = path + "*.png";             // 用于读取目标保存文件里有多少图片
public:
    void input(vector<string> &imageFiles, vector<string> &txtFiles, vector<string> &goalFiles);
    string output();
    Config();

    void TRTknow(vector<string>&orig,vector<string>&goalFiles);
    void TRTpath(string &firstpath ,string &secondPath,string &videoInput,string &Eng, string &Onx);
};