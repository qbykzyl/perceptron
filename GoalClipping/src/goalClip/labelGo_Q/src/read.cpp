#include "../include/read.h"
using namespace std;
Read::Read()
{
    Config config;
    outpath = config.output();
    config.input(InputFiles, Inputtxt, writeFiles);
    namedWindow("test", WINDOW_NORMAL);
    createTrackbar("speed", "test", speed, 5);
    
    time += writeFiles.size();
}
Read::~Read()
{
    cv::destroyWindow("test");
}
void Read::spin()
{
    clip();
}
void Read::clip()
{
    for (int flag = 0; flag < Inputtxt.size(); flag++)
    {
        char line[5];
        cout << "open:" << Inputtxt[flag] << endl;
        cout << "open:" << InputFiles[flag] << endl;
        cout<<">>>>"<<endl;
        ifstream locate;
        double num;
        string s;
        Mat src = imread(InputFiles[flag]);
        locate.open(Inputtxt[flag]);
        while (getline(locate, s))
        {
            stringstream ss(s);
            vector<double> vec;
            int mode = getTrackbarPos("speed", "test");
            if (mode == 0)
                return;    
            if (mode == 1)
                waitTime = 0;
            if (mode == 2)
                waitTime = 3000;
            if (mode == 3)
                waitTime = 2000;
            if (mode == 4)
                waitTime = 1000;
            if (mode == 5)
                waitTime = 50;
            while (ss >> num)
            {
                vec.push_back(num);
            }

            int ox = float(src.cols) * vec[1];
            int oy = float(src.rows) * vec[2];
            int ow = float(src.cols) * vec[3];
            int oh = float(src.rows) * vec[4];
            vector<int> rect;
            int fx = (ox - ow / 2);
            int fy = (oy - oh / 2);
            cout << "[" << fx << "," << fy << "," << ow << "," << oh << "]"
                 << "->" << endl;
            Mat dest = src(Rect(fx, fy, ow, oh));
            resize(dest, dest, Size(640, 640), 0, 0, INTER_CUBIC);
            resizeWindow("test", 710, 670);
            imshow("test", dest);
            if (waitKey(waitTime) != 'd')
            {
                string OutputPath = outpath + to_string(time) + ".png";
                imwrite(OutputPath, dest);
                cout << "Save->" << OutputPath << endl;
                time++;
            }
            else
            {
                cout << "Being->delate" << endl;
            }
        }
        cout << "---------------" << endl;
        locate.close();
    }
    // waitKey(0);
}
