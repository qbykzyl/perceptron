#include <iostream>
#include <opencv2/opencv.hpp>
#include <vector>
using namespace std;
using namespace cv;
class Perceptron

{
    vector<double> weights;
    double bias;

public:
    Perceptron(int numInputs)
    {
        weights.resize(numInputs, 0.0);
        bias = 0.0;
    }

    int activite(vector<double> &inputs)
    {
        double sum = 0.0;
        for (int j = 0; j < inputs.size(); j++)
        {
            sum += inputs[j] * weights[j];
        }
        sum += bias;
        return (sum >= 0) ? 1 : -1;
    }
    bool train(vector<vector<double>> &trainingData, vector<int> &targets, double learningRate)
    {
        int time = 0;
        while (true)
        {
            //Mat src=Mat::zeros(Size(10,10),CV_8UC3);
            //namedWindow("test", WINDOW_NORMAL);
            time++;
            int flag = 0;

            for (int i = 0; i < targets.size(); i++)
            {

                vector<double> inputs = trainingData[i];
                int target = targets[i];
                //circle(src,Point(inputs[0],inputs[1]),1,Scalar(0,200,200),1,8,0);
                if (target !=  activite(inputs))
                {
                    flag++;
                    for (int k = 0; k < weights.size(); ++k)
                    {
                        weights[k] += learningRate * target * inputs[k];
                    }
                    bias += learningRate * target;
                    cout<<flag<<endl;
                }
                //resize(src, src, Size(640, 640), 0, 0, INTER_CUBIC);
                //imshow("test",src);
                //waitKey(100);
            }
            if (!flag)
            {
                
                return 1;
            }
            if (time == 100)
            {
                return 0;
            }
        }
    }
};
int main()
{
    int numInputs = 2;
    Perceptron perceptron(numInputs);
    vector<vector<double>> trainingData = {{0, 0}, {0, 2}, {2, 0}, {2, 2}};
    vector<int> targets = {-1, -1, -1, 1};
    if (perceptron.train(trainingData, targets, 0.1))
    {
        cout << "训练成功" << endl;
    }
    else
    {
        cout << "训练失败" << endl;
        return 0;
    }
    cout << "开始测试" << endl;
    vector<double> testInput ;
    for (int i = 0; i < 2; i++)
    {
        int an;
        cin >> an;
        testInput.push_back(an);
    }
    int prediction = perceptron.activite(testInput);
    cout << "类别为" << prediction << endl;
    return 0;
}